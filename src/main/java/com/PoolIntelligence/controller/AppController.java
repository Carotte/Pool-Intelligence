package com.PoolIntelligence.controller;

import com.PoolIntelligence.model.Player;
import com.PoolIntelligence.service.PlayerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by etien on 2017-05-07.
 */

@RestController
public class AppController {

    @Autowired
    PlayerService playerService;

    @RequestMapping("/list")
    public Player listPlayers() {
        Player players = playerService.findById(1L);
        return players;
    }
}
